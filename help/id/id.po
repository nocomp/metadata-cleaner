# liimee <alt3753.7@gmail.com>, 2021.
# Reza Almanda <rezaalmanda27@gmail.com>, 2021.
# Romain Vigier <romain@romainvigier.fr>, 2021.
# neko <smurfkandy@tutanota.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2021-10-24 23:48+0200\n"
"PO-Revision-Date: 2021-12-24 03:51+0000\n"
"Last-Translator: neko <smurfkandy@tutanota.com>\n"
"Language-Team: Indonesian <https://hosted.weblate.org/projects/"
"metadata-cleaner/help/id/>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 4.10.1\n"

#. (itstool) path: section/title
#: C/general.page:14
msgid "Metadata and privacy"
msgstr "Metadata dan privasi"

#. (itstool) path: section/p
#: C/general.page:15
msgid ""
"Metadata consists of information that characterizes data. Metadata is used "
"to provide documentation for data products. In essence, metadata answers "
"who, what, when, where, why, and how about every facet of the data that is "
"being documented."
msgstr ""
"Metadata terdiri dari informasi yang mencirikan data. Metadata digunakan "
"untuk menyediakan dokumentasi untuk produk data. Intinya, metadata menjawab "
"siapa, apa, kapan, di mana, mengapa, dan bagaimana setiap aspek data yang "
"didokumentasikan."

#. (itstool) path: section/p
#: C/general.page:16
msgid ""
"Metadata within a file can tell a lot about you. Cameras record data about "
"when and where a picture was taken and which camera was used. Office "
"applications automatically add author and company information to documents "
"and spreadsheets. This is sensitive information and you may not want to "
"disclose it."
msgstr ""
"Metadata dalam file dapat memberi tahu banyak tentang Anda. Kamera merekam "
"data tentang kapan dan di mana gambar diambil dan kamera mana yang "
"digunakan. Aplikasi Office secara otomatis menambahkan informasi penulis dan "
"perusahaan ke dokumen dan spreadsheet. Ini adalah informasi sensitif dan "
"Anda mungkin tidak ingin mengungkapkannya."

#. (itstool) path: figure/title
#: C/general.page:18
msgid "Example of metadata in a picture file"
msgstr "Contoh metadata dari berkas gambar"

#. (itstool) path: figure/desc
#: C/general.page:20
msgid ""
"The file discloses information about the hardware, settings, date, location "
"of the shoot. It contains a total of 79 metadata."
msgstr ""
"File tersebut mengungkapkan informasi tentang perangkat keras, pengaturan, "
"tanggal, lokasi pemotretan. File tersebut berisi total 79 metadata."

#. (itstool) path: section/title
#: C/general.page:27
msgid "Cleaning process"
msgstr "Proses pembersihan"

#. (itstool) path: section/p
#: C/general.page:28
msgid ""
"While <app>Metadata Cleaner</app> is doing its very best to display "
"metadata, it doesn't mean that a file is clean from metadata if it doesn't "
"show any. There is no reliable way to detect every single possible metadata "
"for complex file formats. This is why you shouldn't rely on metadata's "
"presence to decide if your file must be cleaned or not."
msgstr ""
"Meskipun <app>Metadata Cleaner</app> melakukan yang terbaik untuk "
"menampilkan metadata, itu tidak berarti bahwa sebuah file bersih dari "
"metadata jika tidak ditampilkan. Tidak ada cara yang dapat diandalkan untuk "
"mendeteksi setiap kemungkinan metadata untuk format file yang kompleks. "
"Inilah sebabnya mengapa Anda tidak boleh bergantung pada keberadaan metadata "
"untuk memutuskan apakah file Anda harus dibersihkan atau tidak."

#. (itstool) path: section/p
#: C/general.page:29
msgid ""
"<app>Metadata Cleaner</app> takes the content of the file and puts it into a "
"new one without metadata, ensuring that any undetected metadata is stripped."
msgstr ""
"<app>Metadata Cleaner</app> mengambil konten dari file dan memasukkannya ke "
"file baru tanpa metadata, memastikan metadata yang tidak terdeteksi dihapus."

#. (itstool) path: section/title
#: C/general.page:35
msgid "Limitations"
msgstr "Batasan"

#. (itstool) path: section/p
#: C/general.page:36
msgid ""
"Be aware that metadata is not the only way of marking a file. If the content "
"itself discloses personal information or has been watermarked, traditionally "
"or via steganography, <app>Metadata Cleaner</app> will not protect you."
msgstr ""
"Ketahuilah bahwa metadata bukan satu-satunya cara menandai file. Jika konten "
"itu sendiri mengungkapkan informasi pribadi atau telah diberi watermark, "
"secara tradisional atau melalui steganografi, <app>Metadata Cleaner</app> "
"tidak dapat melindungi Anda."

#. (itstool) path: page/title
#. The application name. It can be translated.
#: C/index.page:10
msgid "Metadata Cleaner"
msgstr "Pembersih Metadata"

#. (itstool) path: page/p
#: C/index.page:14
msgid ""
"<app>Metadata Cleaner</app> allows you to view metadata in your files and to "
"get rid of it, as much as possible."
msgstr ""
"<app> Metadata Cleaner </app> memungkinkan Anda untuk melihat metadata dalam "
"file Anda danmenyingkirkannya, sebanyak mungkin."

#. (itstool) path: section/title
#: C/index.page:17
msgid "General information"
msgstr "Informasi Umum"

#. (itstool) path: section/title
#: C/index.page:21
msgid "Using <app>Metadata Cleaner</app>"
msgstr "Menggunakan <app>Metadata Cleaner</app>"

#. (itstool) path: section/title
#: C/usage.page:14
msgid "Adding files"
msgstr "Menambah berkas"

#. (itstool) path: section/p
#: C/usage.page:15
msgid ""
"To add files, press the <gui style=\"button\">Add Files</gui> button. A file "
"chooser will open, select the files you want to clean."
msgstr ""
"Untuk menambahkan berkas, tekan tombol <gui style=\"button\">Tambah berkas</"
"gui>. Pemilih berkas akan terbuka, pilih berkas yang ingin Anda bersihkan."

#. (itstool) path: figure/title
#: C/usage.page:17
msgid "<gui style=\"button\">Add Files</gui> button"
msgstr "Tombol <gui style=\"button\">Tambah File</gui>"

#. (itstool) path: section/p
#: C/usage.page:20
msgid ""
"To add whole folders at once, press the arrow next to the <gui style=\"button"
"\">Add Files</gui> button and press the <gui style=\"button\">Add Folders</"
"gui> button. A file chooser will open, select the folders you want to add. "
"You can optionally choose to also add all files from all subfolders by "
"checking the <gui style=\"checkbox\">Add files from subfolders</gui> "
"checkbox."
msgstr ""
"Untuk menambahkan seluruh folder sekaligus, tekan panah di sebelah tombol "
"<gui style=\"button\">Tambah File</gui> dan tekan tombol <gui style=\"button"
"\">Tambah Folder</gui>. Sebuah pemilih file akan terbuka, pilih folder yang "
"ingin Anda tambahkan. Anda juga dapat memilih untuk menambahkan semua file "
"dari semua subfolder dengan mencentang kotak <gui style=\"checkbox\">Tambah "
"file dari subfolder</gui>."

#. (itstool) path: section/title
#: C/usage.page:26
msgid "Viewing metadata"
msgstr "Melihat metadata"

#. (itstool) path: section/p
#: C/usage.page:27
msgid ""
"Click on a file in the list to open the detailed view. If it has metadata, "
"it will be shown there."
msgstr ""
"Klik pada file dalam daftar untuk membuka tampilan rinci. Jika terdapat "
"metadata, ia akan ditampilkan di sana."

#. (itstool) path: figure/title
#: C/usage.page:29
msgid "Detailed view of the metadata"
msgstr "Tampilan rinci dari metadata"

#. (itstool) path: section/title
#: C/usage.page:37
msgid "Cleaning files"
msgstr "Membersihkan berkas"

#. (itstool) path: section/p
#: C/usage.page:38
msgid ""
"To clean all the files in the window, press the <gui style=\"button\">Clean</"
"gui> button. The cleaning process may take some time to complete."
msgstr ""
"Untuk membersihkan semua file di jendela, tekan tombol <gui style=\"button\""
">Bersihkan</gui>. Proses pembersihan mungkin membutuhkan waktu untuk "
"diselesaikan."

#. (itstool) path: figure/title
#: C/usage.page:40
msgid "<gui style=\"button\">Clean</gui> button"
msgstr "Tombol <gui style=\"button\">Bersihkan</gui>"

#. (itstool) path: section/title
#: C/usage.page:48
msgid "Lightweight cleaning"
msgstr "Pembersihan ringan"

#. (itstool) path: section/p
#: C/usage.page:49
msgid ""
"By default, the removal process might alter a bit the data of your files, in "
"order to remove as much metadata as possible. For example, texts in PDF "
"might not be selectable anymore, compressed images might get compressed "
"again…"
msgstr ""
"Secara default, proses penghapusan mungkin mengubah sedikit data file Anda, "
"untuk menghapus metadata sebanyak mungkin. Misalnya, teks dalam PDF mungkin "
"tidak dapat dipilih lagi, gambar yang dikompresi mungkin akan dikompresi "
"lagi…"

#. (itstool) path: section/p
#: C/usage.page:50
msgid ""
"The lightweight mode, while not as thorough, will not make destructive "
"changes to your files."
msgstr ""
"Mode ringan, meskipun tidak menyeluruh, tidak akan membuat perubahan "
"destruktif pada file Anda."
