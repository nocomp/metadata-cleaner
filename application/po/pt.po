# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the fr.romainvigier.MetadataCleaner package.
# Juliano de Souza Camargo <julianosc@protonmail.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: fr.romainvigier.MetadataCleaner\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-10 18:05+0100\n"
"PO-Revision-Date: 2022-01-08 13:40+0000\n"
"Last-Translator: Juliano de Souza Camargo <julianosc@protonmail.com>\n"
"Language-Team: Portuguese <https://hosted.weblate.org/projects/"
"metadata-cleaner/application/pt/>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 4.10.1\n"

#: application/data/fr.romainvigier.MetadataCleaner.desktop.in:6
#: application/data/fr.romainvigier.MetadataCleaner.desktop.in:7
#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:9
#: application/data/ui/AboutDialog.ui:76 application/metadatacleaner/app.py:38
msgid "Metadata Cleaner"
msgstr "Limpador de metadados"

#: application/data/fr.romainvigier.MetadataCleaner.desktop.in:8
msgid "Clean metadata from your files"
msgstr "Limpar os metadados de seus ficheiros"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: application/data/fr.romainvigier.MetadataCleaner.desktop.in:16
msgid "Metadata;Remover;Cleaner;"
msgstr "Metadado;Metainformação;Eliminar;Remover;Limpador;"

#: application/data/fr.romainvigier.MetadataCleaner.gschema.xml:10
msgid "Clean without warning"
msgstr "Limpar sem advertir"

#: application/data/fr.romainvigier.MetadataCleaner.gschema.xml:11
msgid "Clean the files without showing the warning dialog"
msgstr "Limpar os ficheiros sem mostrar o diálogo de advertência"

#: application/data/fr.romainvigier.MetadataCleaner.gschema.xml:15
msgid "Lightweight cleaning"
msgstr "Limpeza ligeira"

#: application/data/fr.romainvigier.MetadataCleaner.gschema.xml:16
msgid "Don't make destructive changes to files but may leave some metadata"
msgstr ""
"Não faz alterações destrutivas nos ficheiros mas pode ignorar algum metadado"

#: application/data/fr.romainvigier.MetadataCleaner.gschema.xml:20
msgid "Window width"
msgstr "Largura da janela"

#: application/data/fr.romainvigier.MetadataCleaner.gschema.xml:21
msgid "Saved width of the window"
msgstr "Largura gravada da janela"

#: application/data/fr.romainvigier.MetadataCleaner.gschema.xml:25
msgid "Window height"
msgstr "Altura da janela"

#: application/data/fr.romainvigier.MetadataCleaner.gschema.xml:26
msgid "Saved height of the window"
msgstr "Altura gravada da janela"

#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:10
msgid "View and clean metadata in files"
msgstr "Ver e limpar os metadados dos ficheiros"

#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:27
msgid ""
"Metadata within a file can tell a lot about you. Cameras record data about "
"when and where a picture was taken and which camera was used. Office "
"applications automatically add author and company information to documents "
"and spreadsheets. This is sensitive information and you may not want to "
"disclose it."
msgstr ""
"Os metadados dum ficheiro podem dizer muito sobre alguém. As câmaras "
"registam dados sobre quando e onde se tomou uma foto e que câmara se usou. "
"As aplicações de escritório registam automaticamente informação acerca do "
"autor e da empresa nos documentos e folhas de cálculo. Trata-se de "
"informação delicada e é possível que não a queira revelar."

#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:28
msgid ""
"This tool allows you to view metadata in your files and to get rid of it, as "
"much as possible."
msgstr ""
"Esta ferramenta permite-lhe ver os metadados nos seus ficheiros e desfazer-"
"se deles na medida do possível."

#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:75
msgid "Bug fixes"
msgstr "Correção de erros"

#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:76
#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:85
#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:93
#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:112
#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:120
#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:128
#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:139
#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:150
#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:159
msgid "New translations"
msgstr "Novas traduções"

#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:84
msgid "New button to add folders"
msgstr "Novo botão para adicionar pastas"

#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:101
msgid "Improved adaptive user interface"
msgstr "Interface de utilizador adaptável aprimorada"

#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:102
msgid "New help pages"
msgstr "Novas páginas de ajuda"

#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:103
msgid "One-click cleaning, no need to save after cleaning"
msgstr "Limpar com um clique, não há necessidade de gravar após a limpeza"

#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:104
msgid "Persistent lightweight cleaning option"
msgstr "Opção de limpeza ligeira persistente"

#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:151
msgid "Files with uppercase extension can now be added"
msgstr "Agora pode-se adicionar ficheiros com extensão em maiúsculas"

#: application/data/gtk/help-overlay.ui:15
msgid "Files"
msgstr "Ficheiros"

#: application/data/gtk/help-overlay.ui:18
msgid "Add files"
msgstr "Adicionar ficheiros"

#: application/data/gtk/help-overlay.ui:24
msgid "Add folders"
msgstr "Adicionar pastas"

#: application/data/gtk/help-overlay.ui:30
msgid "Clean metadata"
msgstr "Limpar metadados"

#: application/data/gtk/help-overlay.ui:36
msgid "Clear all files from window"
msgstr "Limpar todos os ficheiros da janela"

#: application/data/gtk/help-overlay.ui:44
msgid "General"
msgstr "Geral"

#: application/data/gtk/help-overlay.ui:47
msgid "New window"
msgstr "Nova janela"

#: application/data/gtk/help-overlay.ui:53
msgid "Close window"
msgstr "Fechar janela"

#: application/data/gtk/help-overlay.ui:59
msgid "Quit"
msgstr "Sair"

#: application/data/gtk/help-overlay.ui:65
msgid "Keyboard shortcuts"
msgstr "Atalhos de teclado"

#: application/data/gtk/help-overlay.ui:71
msgid "Help"
msgstr "Ajuda"

#: application/data/ui/AboutDialog.ui:97
msgid "Chat on Matrix"
msgstr "Conversar em Matrix"

#: application/data/ui/AboutDialog.ui:107
msgid "View the code on GitLab"
msgstr "Ver o código em GitLab"

#: application/data/ui/AboutDialog.ui:117
msgid "Translate on Weblate"
msgstr "Traduzir em Weblate"

#: application/data/ui/AboutDialog.ui:127
msgid "Support us on Liberapay"
msgstr "Apoie-nos em Liberapay"

#: application/data/ui/AboutDialog.ui:170
msgid "Code"
msgstr "Código"

#: application/data/ui/AboutDialog.ui:176
msgid "Artwork"
msgstr "Arte"

#: application/data/ui/AboutDialog.ui:182
msgid "Documentation"
msgstr "Documentação"

#: application/data/ui/AboutDialog.ui:188
msgid "Translation"
msgstr "Tradução"

#: application/data/ui/AboutDialog.ui:193
msgid ""
"This program uses <a href=\"https://0xacab.org/jvoisin/mat2\">mat2</a> to "
"parse and clean the metadata. Show them some love!"
msgstr ""
"Este programa usa <a href=\"https://0xacab.org/jvoisin/mat2\">mat2</a> para "
"analisar e limpar os metadados. Ofereça-lhes um pouco de amor!"

#: application/data/ui/AboutDialog.ui:206
msgid ""
"The source code of this program is released under the terms of the <a href="
"\"https://www.gnu.org/licenses/gpl-3.0.html\">GNU GPL 3.0 or later</a>. The "
"original artwork and translations are released under the terms of the <a "
"href=\"https://creativecommons.org/licenses/by-sa/4.0/\">CC BY-SA 4.0</a>."
msgstr ""
"O código-fonte deste programa é publicado sob os termos da <a href=\"https"
"://www.gnu.org/licenses/gpl-3.0.html\">GNU GPL 3.0 ou posterior</a>. O "
"material gráfico e as traduções são publicadas sob os termos <a href=\"https"
"://creativecommons.org/licenses/by-sa/4.0/\">CC BY-SA 4.0</a>."

#: application/data/ui/AddFilesButton.ui:11
msgid "_Add Files"
msgstr "_Adicionar ficheiros"

#: application/data/ui/AddFilesButton.ui:24
msgid "Add _Folders"
msgstr "Adicionar _pastas"

#: application/data/ui/CleanMetadataButton.ui:9
msgid "_Clean"
msgstr "_Limpar"

#: application/data/ui/CleaningWarningDialog.ui:9
msgid "Make sure you backed up your files!"
msgstr "Assegure-se de fazer uma cópia de segurança dos seus ficheiros!"

#: application/data/ui/CleaningWarningDialog.ui:10
msgid "Once the files are cleaned, there's no going back."
msgstr "Uma vez limpados os ficheiros, não há volta atrás."

#: application/data/ui/CleaningWarningDialog.ui:17
msgid "Don't tell me again"
msgstr "Não voltar a mostrar esta mensagem"

#: application/data/ui/CleaningWarningDialog.ui:25
msgid "Cancel"
msgstr "Cancelar"

#: application/data/ui/CleaningWarningDialog.ui:30
msgid "Clean"
msgstr "Limpar"

#: application/data/ui/EmptyView.ui:13
msgid "Clean Your Traces"
msgstr "Limpe as suas pegadas"

#: application/data/ui/EmptyView.ui:38
msgid "Learn more about metadata and the cleaning process limitations."
msgstr ""
"Mais informação acerca dos metadados e das limitações do processo de limpeza."

#: application/data/ui/FileRow.ui:12
msgid "Remove file from list"
msgstr "Remover ficheiro da lista"

#: application/data/ui/FileRow.ui:124
msgid "Warning"
msgstr "Advertência"

#: application/data/ui/FileRow.ui:142
msgid "Error"
msgstr "Erro"

#: application/data/ui/FileRow.ui:182
msgid "Cleaned"
msgstr "Limpo"

#: application/data/ui/MenuButton.ui:10
msgid "_New Window"
msgstr "_Nova janela"

#: application/data/ui/MenuButton.ui:14
msgid "_Clear Window"
msgstr "_Limpar janela"

#: application/data/ui/MenuButton.ui:20
msgid "_Help"
msgstr "A_juda"

#: application/data/ui/MenuButton.ui:25
msgid "_Keyboard Shortcuts"
msgstr "Atalhos de _teclado"

#: application/data/ui/MenuButton.ui:29
msgid "_About Metadata Cleaner"
msgstr "Acerca do Limpador de _metadados"

#: application/data/ui/SettingsButton.ui:9
msgid "Cleaning settings"
msgstr "Ajustes de limpeza"

#: application/data/ui/SettingsButton.ui:27
msgid "Lightweight Cleaning"
msgstr "Limpeza ligeira"

#: application/data/ui/SettingsButton.ui:32
msgid "Learn more about the lightweight cleaning"
msgstr "Mais informação acerca da limpeza ligeira"

#: application/data/ui/Window.ui:78
msgid "Details"
msgstr "Detalhes"

#: application/data/ui/Window.ui:84
msgid "Close"
msgstr "Fechar"

#. Translators: Replace `translator-credits` by your name, and optionally your email address between angle brackets (Example: `Name <mail@example.org>`). If names are already present, do not remove them and add yours on a new line.
#: application/data/ui/Window.ui:104
msgid "translator-credits"
msgstr "Juliano de Souza Camargo <julianosc@pm.me>"

#: application/data/ui/Window.ui:113
msgid "Choose files to clean"
msgstr "Selecione os ficheiros que deseja limpar"

#: application/data/ui/Window.ui:123
msgid "Choose folders to clean"
msgstr "Selecione as pastas que deseja limpar"

#: application/metadatacleaner/modules/file.py:254
msgid "Something bad happened during the cleaning, cleaned file not found"
msgstr "Algo correu mal durante a limpeza, ficheiro limpo não encontrado"

#: application/metadatacleaner/ui/detailsview.py:58
msgid "The file has been cleaned."
msgstr "Limpou-se o ficheiro."

#: application/metadatacleaner/ui/detailsview.py:69
msgid "Unable to read the file."
msgstr "Não foi possível ler o ficheiro."

#: application/metadatacleaner/ui/detailsview.py:70
msgid "File type not supported."
msgstr "Tipo de ficheiro não compatível."

#: application/metadatacleaner/ui/detailsview.py:72
msgid "Unable to check metadata."
msgstr "Não foi possível verificar os metadados."

#: application/metadatacleaner/ui/detailsview.py:74
msgid "No known metadata, the file will be cleaned to be sure."
msgstr "Não há metadados conhecidos, limpar-se-á o ficheiro para assegurar-se."

#: application/metadatacleaner/ui/detailsview.py:76
msgid "Unable to remove metadata."
msgstr "Não foi possível eliminar os metadados."

#: application/metadatacleaner/ui/filechooserdialog.py:24
msgid "All supported files"
msgstr "Todos os ficheiros compatíveis"

#: application/metadatacleaner/ui/folderchooserdialog.py:22
msgid "Add files from subfolders"
msgstr "Adicionar ficheiros de subpastas"

#: application/metadatacleaner/ui/statusindicator.py:36
msgid "Adding files…"
msgstr "A adicionar ficheiros…"

#: application/metadatacleaner/ui/statusindicator.py:38
msgid "Processing file {}/{}"
msgstr "A processar ficheiro {}/{}"

#: application/metadatacleaner/ui/statusindicator.py:40
msgid "Cleaning file {}/{}"
msgstr "A limpar ficheiro {}/{}"
